#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    plotPointCount = 1200;
    plotX = 0;
    mainTemperature = 0;
    power = 0;

    // Инициализация графика
    ui->plot->addGraph();
    ui->plot->yAxis->setLabel(QString::fromUtf8("Температура"));
    ui->plot->xAxis->setLabel(QString::fromUtf8("Время"));
    ui->plot->yAxis->setRangeLower(0);
    ui->plot->yAxis->setRangeUpper(110);
    ui->plot->graph()->setPen(QPen(QColor(121, 167, 51), 2));
    ui->plot->graph()->setAntialiased(false);
    for (int i = 0; i < plotPointCount; i++) {
        ui->plot->graph()->addData(plotX++, 0);
    }
    ui->plot->graph()->rescaleKeyAxis();
    ui->plot->replot();

    // Загрузка конфига
    config = new HardwareConfig();

    // Запуск опроса датчиков в потоке
    sensorReader.moveToThread(&sensorThread);
    connect(&sensorThread, SIGNAL(started()), &sensorReader, SLOT(process()));
    connect(&sensorReader, SIGNAL(newData()), this, SLOT(onSensorReaderData()));
    sensorThread.start();
    timer = startTimer(1000);

    // Заполнение заголовка таблички
    ui->tempGrid->setColumnCount(config->sensors.count());
    QColor color;
    QFont titleFont;
    titleFont.setBold(false);
    titleFont.setPointSize(10);
    for (int i = 0; i < config->sensors.count(); i++) {
        QTableWidgetItem * item = new QTableWidgetItem();
        item->setText(config->sensors[i].name);
        item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        item->setFont(titleFont);
        color.setNamedColor(config->sensors[i].color);
        item->setBackgroundColor(color);
        ui->tempGrid->setItem(0,i,item);

        cells[config->sensors[i].id] = new QTableWidgetItem();
        cells[config->sensors[i].id]->setText("0");
        cells[config->sensors[i].id]->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        cells[config->sensors[i].id]->setBackgroundColor(color);
        ui->tempGrid->setItem(1,i,cells[config->sensors[i].id]);

        if (config->sensors[i].isMain) {
            mainSensorId = config->sensors[i].id;
        }
    }

    // Дорисовка таблички
    QPalette pal=palette();
    pal.setColor(QPalette::Base,Qt::transparent);
    ui->tempGrid->setPalette(pal);
    ui->tempGrid->setRowHeight(1, 60);

    // Запуск потока PWM нагревателя
    heater = new Heater(this);
    heater->start();
    heater->pin = config->heaterPin;

    //setWindowTitle(QApplication::applicationDirPath());
}

/**
 * @brief Callback получения данных датчиков
 */
void MainWindow::onSensorReaderData()
{
    QMap <QString, double> values = sensorReader.getValues();
    if (values.count() == 0) {
        return;
    }
    QList<QString> ids = values.keys();
    for (int i = 0; i < ids.count(); i++) {
        if (cells.contains(ids[i])) {
            cells[ids[i]]->setText(QString::number(values[ids[i]], 'f', 1));
            if (mainSensorId == ids[i]) {
                mainTemperature = values[ids[i]];
            }
        }
    }
}

/**
 * @brief Деструктор
 */
MainWindow::~MainWindow()
{
    heater->quit();

    QProcess process;
    process.start("gpio", QStringList() << "write" << QString::number(config->heaterPin) << "0");
    QString(process.readAllStandardOutput());

    sensorReader.stop = true;
    killTimer(timer);
    sensorThread.terminate();
    sensorThread.wait();
    delete config;
    delete ui;
}

/**
 * @brief Событие таймера
 */
void MainWindow::timerEvent(QTimerEvent *)
{
    // дополняю график
    int count = ui->plot->graph()->data()->count();
    if (count > plotPointCount) {
        ui->plot->graph()->removeDataBefore(plotX - plotPointCount);
    }
    ui->plot->graph()->addData(plotX++, mainTemperature);
    ui->plot->graph()->rescaleKeyAxis();
    ui->plot->replot();
}

void MainWindow::changePower()
{
    ui->powerLabel->setText(QString::number(power));
    heater->power = power;
}

void MainWindow::on_powerDown_clicked()
{
    if (power > 0) {
        power--;
        changePower();
    }
}

void MainWindow::on_powerUp_clicked()
{
    if (power < 100) {
        power++;
        changePower();
    }
}
