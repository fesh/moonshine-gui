#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <QString>
#include <QDomNode>

class ConfigParser
{
public:
    ConfigParser();
protected:
    QString selectXmlValue(QDomNode node, QString innerNodeName);
};

#endif // CONFIGPARSER_H
