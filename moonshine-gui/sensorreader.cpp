#include "sensorreader.h"
#include <QProcess>
#include <QDir>
#include <QDirIterator>
#include <QTime>
#include <QCoreApplication>

SensorReader::SensorReader()
{
    stop = false;
}

void SensorReader::process()
{
//    double temp = 23;
    while(!stop) {
        QStringList all_dirs = QDir("/sys/bus/w1/devices", "28*").entryList();
        QString fileName;
        for (int i = 0; i < all_dirs.count(); i++) {
            QString sensorName(all_dirs[i]);
            fileName = all_dirs[i].prepend("/sys/bus/w1/devices/").append("/w1_slave");
            QFile file(fileName);
            if (file.open(QIODevice::ReadOnly)) {
                QString text(file.readAll());
                QRegExp regex("t=(\\d+)");
                if (regex.indexIn(text, 0) != -1) {
                    mutex.lock();
                    values[sensorName] = regex.cap(1).toDouble() / 1000.0;
                    mutex.unlock();
                }
                file.close();
            }
        }

        // callback в контроллер
        emit(newData());

//        values[QString("28-0000068a19f0")] = temp;
//        temp += 0.1;
//        emit(newData());

//        QTime dieTime = QTime::currentTime().addMSecs(500);
//        while( QTime::currentTime() < dieTime ) {
//            QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
//        }
    }
}

QMap <QString, double> SensorReader::getValues()
{
    mutex.lock();
    QMap <QString, double> result(values);
    mutex.unlock();

    return result;
}
