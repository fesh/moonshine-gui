#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QTableWidgetItem>
#include "sensorreader.h"
#include "hardwareconfig.h"
#include "heater.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onSensorReaderData();

private slots:
    void on_powerDown_clicked();

    void on_powerUp_clicked();

private:
    Ui::MainWindow *ui;

    void timerEvent(QTimerEvent *event);
    void changePower();

    int timer;
    int plotPointCount;
    int plotX;
    int power;
    double mainTemperature;
    QThread sensorThread;
    SensorReader sensorReader;
    QMap <QString, QTableWidgetItem *> cells;
    QString mainSensorId;
    HardwareConfig *config;
    Heater *heater;
};

#endif // MAINWINDOW_H
