#include "hardwareconfig.h"
#include "QApplication"

/**
 * @brief Конструктор
 */
HardwareConfig::HardwareConfig()
{
    QFile *file = new QFile(QApplication::applicationDirPath().append("/config.xml"));
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }

    QDomDocument *xml = new QDomDocument();
    xml->setContent(file);

    QDomNodeList nodeList = xml->documentElement().elementsByTagName("heater");
    QDomNodeList pinList;

    // Выход нагревателя
    if (nodeList.count() > 0) {
        pinList = nodeList.at(0).toElement().elementsByTagName("pin");
        if (pinList.count() > 0) {
            heaterPin = pinList.at(0).toElement().text().toInt();
        }
    }

    // Датчики
    nodeList = xml->documentElement().elementsByTagName("sensor");
    for (int i = 0; i < nodeList.count(); i++) {
        SensorConfig sensor;
        sensor.name = this->selectXmlValue(nodeList.at(i), "name");
        sensor.id = this->selectXmlValue(nodeList.at(i), "id");
        sensor.color = this->selectXmlValue(nodeList.at(i), "color");
        sensor.isMain = this->selectXmlValue(nodeList.at(i), "main").contains("true");
        sensors.append(sensor);
    }

    file->close();
    delete file;
    delete xml;
}
