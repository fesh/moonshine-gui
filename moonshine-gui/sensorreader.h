#ifndef SENSORREADER_H
#define SENSORREADER_H

#include <QObject>
#include <QMutex>
#include <QMap>

class SensorReader : public QObject
{
Q_OBJECT

private:
    QMutex mutex;
    QMap <QString, double> values;

public:
    SensorReader();
    QMap <QString, double> getValues();
    bool stop;

public slots:
    void process();

signals:
    void newData();
};

#endif // SENSORREADER_H
