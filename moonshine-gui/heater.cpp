#include "heater.h"
#include <QProcess>

Heater::Heater(QObject *parent) :
    QThread(parent)
{
    power = 0;
}


void Heater::run() {
    double totalMS = 3 * 1000;
    double pause = 0;
    double impulse = 0;

    QProcess process1;
    QProcess process0;

    while (true) {
        impulse = totalMS / 100.0 * power;
        pause = totalMS / 100.0 * (100 - power);

        // Импульс = 1
        if (impulse > 0) {
            process1.start("gpio", QStringList() << "write" << QString::number(pin) << "1");
            process1.waitForFinished();
            this->msleep(impulse);
        }

        // Пауза = 0
        process0.start("gpio", QStringList() << "write" << QString::number(pin) << "0");
        process0.waitForFinished();
        this->msleep(pause);
    }
}
