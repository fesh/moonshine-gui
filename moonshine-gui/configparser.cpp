#include "configparser.h"

ConfigParser::ConfigParser()
{
}

/**
 * @brief ConfigParser::selectXmlValue
 * @param node
 * @param innerNodeName
 * @return
 */
QString ConfigParser::selectXmlValue(QDomNode node, QString innerNodeName)
{
    QDomNodeList nodeList = node.toElement().elementsByTagName(innerNodeName);
    if (nodeList.count() == 0) {
        return QString("");
    }
    return nodeList.at(0).toElement().text();
}
