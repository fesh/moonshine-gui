#ifndef HEATER_H
#define HEATER_H

#include <QThread>

class Heater : public QThread
{
    Q_OBJECT

private:
    void run();

public:
    int power;
    int pin;
    explicit Heater(QObject *parent = 0);

signals:

public slots:

};

#endif // HEATER_H
