#ifndef SENSORCONFIG_H
#define SENSORCONFIG_H

#include <QString>

class SensorConfig
{
public:
    QString id;
    QString name;
    bool isMain;
    QString color;
    double temperature;

    SensorConfig();
};

#endif // SENSORCONFIG_H
