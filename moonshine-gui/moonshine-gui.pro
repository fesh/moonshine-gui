#-------------------------------------------------
#
# Project created by QtCreator 2016-06-24T11:46:54
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = moonshine-gui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    sensorconfig.cpp \
    sensorreader.cpp \
    hardwareconfig.cpp \
    configparser.cpp \
    heater.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    sensorconfig.h \
    sensorreader.h \
    hardwareconfig.h \
    configparser.h \
    heater.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    config.xml

copyother.commands = $(COPY_DIR) $$PWD/config.xml $$OUT_PWD
# copyother.target = $$OUT_PWD/processes
first.depends = $(first) copyother
export(first.depends)
export(copyother.commands)
QMAKE_EXTRA_TARGETS += first copyother

RESOURCES += \
    resource.qrc
