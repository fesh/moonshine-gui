#ifndef HARD_CONFIG_H
#define HARD_CONFIG_H

#include <QtXml>
#include <QList>
#include "sensorconfig.h"
#include "configparser.h"

class HardwareConfig : ConfigParser
{
public:
    QList <SensorConfig> sensors;
    int heaterPin;

    HardwareConfig();
};

#endif // CONFIG_H
